package main

import (
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/dialog"
	"runtime"
	"net/url"
	"io/ioutil"
	"bytes"
	"strings"
	"net/http"
	"os"
	"fmt"
	Endpoint "gitlab.com/RicardoValladares/api-mysql/SysHost"
	Servidor "gitlab.com/RicardoValladares/api-mysql/Server"
)

var(
	IP = binding.NewString()
	User = binding.NewString()
	Password = binding.NewString()
	Sql *widget.Entry
	curl *widget.Entry
	respuesta *widget.Entry
)


func main() {
	if len(os.Args) == 2 {
		Endpoint.SetPuerto(os.Args[1])
		Servidor.Run(Endpoint.HostName(), Endpoint.Puerto(), Endpoint.Usuario(), Endpoint.Clave())
	} else if len(os.Args) == 1 {
		go Servidor.Run(Endpoint.HostName(), Endpoint.Puerto(), Endpoint.Usuario(), Endpoint.Clave())

		myApp := app.NewWithID("gitlab.com.ricardovalladares.apimysql") //app.New()
		myApp.Settings().SetTheme(theme.DarkTheme())

		myWindow := myApp.NewWindow("Container")
		toolbar := widget.NewToolbar(
			widget.NewToolbarAction(theme.MediaPlayIcon(), func() {
				dialog.ShowInformation("Api Rest", Iniciar(), myWindow)
			}),
			widget.NewToolbarAction(theme.MediaStopIcon(), func() {
				dialog.ShowInformation("Api Rest", Detener(), myWindow)
			}),
			//widget.NewToolbarSeparator(),
			widget.NewToolbarSpacer(),
			widget.NewToolbarAction(theme.HelpIcon(), func() {
				git, _ := url.Parse("https://gitlab.com/RicardoValladares/api-mysql")
				_ =myApp.OpenURL(git)
			}),
		)
		
		

		IP.Set("")
		entry1 := widget.NewEntryWithData(IP)
		entry1.SetPlaceHolder("IP/Database")
		slabel1 := binding.NewString()
		slabel1.Set("http://"+Endpoint.HostName()+"/API/")
		label1 := widget.NewLabelWithData(slabel1)
		Group1 := container.NewGridWithColumns(2,label1, entry1)

		User.Set("")
		entry2 := widget.NewEntryWithData(User)
		entry2.SetPlaceHolder("User")
		Password.Set("")
		entry3 := widget.NewEntryWithData(Password)
		entry3.SetPlaceHolder("Password")
		slabel2 := binding.NewString()
		slabel2.Set("Credenciales:")
		label2 := widget.NewLabelWithData(slabel2)
		Group2 := container.NewGridWithColumns(3,label2, entry2, entry3)

		slabel3 := binding.NewString()
		slabel3.Set("Query:")
		label3 := widget.NewLabelWithData(slabel3)
		Sql := widget.NewMultiLineEntry()
		Sql.SetPlaceHolder("SELECT is_role, TO_BASE64(default_role) \nFROM mysql.user;")

		curl := widget.NewMultiLineEntry()
		curl.SetPlaceHolder("CURL ...")
		
		respuesta := widget.NewMultiLineEntry()
		respuesta.SetPlaceHolder("Response...")

		OnOff := func() {
			curl.SetText("")
			respuesta.SetText("")

			SIP, _ := IP.Get()
			SUser, _ := User.Get()
			SPassword, _ := Password.Get()
			SSql := Sql.Text

			var param = url.Values{}
			param.Set("sql", SSql)
			var payload = bytes.NewBufferString(param.Encode())
			client := &http.Client{}
			req, err := http.NewRequest("POST", "http://"+Endpoint.HostName()+"/API/"+SIP, payload)
			if err != nil {
				dialog.ShowError(err, myWindow)
				return
			}
			req.SetBasicAuth(SUser, SPassword)
			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
			response, err := client.Do(req)
			if err != nil {
				dialog.ShowError(err, myWindow)
				return
			}
			responseData, err := ioutil.ReadAll(response.Body)
			if err != nil {
				dialog.ShowError(err, myWindow)
				return
			}
			respuesta.SetText(string(responseData))
			curl.SetText("curl -X POST 'http://"+Endpoint.HostName()+"/API/"+SIP+"' -H 'Authorization: Basic "+Endpoint.GetCredenciales(SUser, SPassword)+"' -H 'Content-Type: application/x-www-form-urlencoded' -d 'sql="+strings.Replace(SSql, "'", "\"", -1)+"'")
		}
		boton1 := widget.NewButtonWithIcon("Generar consulta", theme.MediaPlayIcon(), OnOff)
		
		if runtime.GOOS == "android" {
			card := widget.NewCard("API Rest", "", container.NewBorder(container.New(layout.NewVBoxLayout(), toolbar, Group1, Group2, label3, Sql, boton1, curl), nil, nil, nil, respuesta) )
			myWindow.SetContent(card)
			myWindow.ShowAndRun()
		} else {
			card := widget.NewCard("API Rest", "",container.New(layout.NewVBoxLayout(), toolbar, Group1, Group2, label3, Sql, boton1, curl))
			content := container.New(layout.NewGridLayout(2), card, respuesta)
			myWindow.SetContent(content)
			myWindow.ShowAndRun()
		}
	} else {
		fmt.Println("Argumentos invalidos")
	}
}

func Detener() string {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "http://"+Endpoint.HostName()+"/Off", nil)
	req.SetBasicAuth(Endpoint.Usuario(), Endpoint.Clave())
	response, err := client.Do(req)
	if err != nil {
		return "Error de comunicacion"
	}
	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "Error de comunicacion"
	}
	return string(responseData)
}

func Iniciar() string {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "http://"+Endpoint.HostName()+"/On", nil)
	req.SetBasicAuth(Endpoint.Usuario(), Endpoint.Clave())
	response, err := client.Do(req)
	if err != nil {
		return "Error de comunicacion"
	}
	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "Error de comunicacion"
	}
	return string(responseData)
}
