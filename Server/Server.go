package Server

import (
	"encoding/base64"
	"database/sql"
	"fmt"
	"net/http"
	"os"
	"runtime"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	Binario "gitlab.com/RicardoValladares/api-mysql/Server/WASM"
)

var (
	StatusServer bool
	HostName     string
	Puerto       string
	Usuario      string
	Clave        string
)

func Run(hostname, puerto, usuario, clave string) {
	HostName = hostname
	Puerto = puerto
	Usuario = usuario
	Clave = clave

	StatusServer = true
	r := mux.NewRouter()

	if runtime.GOOS == "android" {
		r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "<html><head><script>window.location='index.html';</script></head><body><pre><a href='index.html'>index.html</a></pre></body></html>")
		})
		r.HandleFunc("/api-mysql.wasm", Binario.Api)
		r.HandleFunc("/icon.png", Binario.Icon)
		r.HandleFunc("/spinner_dark.gif", Binario.Spinnerdark)
		r.HandleFunc("/spinner_light.gif", Binario.Spinnerlight)
		r.HandleFunc("/index.html", Binario.Index)
		r.HandleFunc("/light.css", Binario.Light)
		r.HandleFunc("/dark.css", Binario.Dark)
		r.HandleFunc("/wasm_exec.js", Binario.Wasm)
		r.HandleFunc("/webgl-debug.js", Binario.Webgl)
	} else {
		if DirExists("./wasm") {
			r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprintf(w, "<html><head><script>window.location='wasm/index.html';</script></head><body><pre><a href='wasm/index.html'>wasm</a></pre></body></html>")
			})
			r.PathPrefix("/wasm/").Handler(http.StripPrefix("/wasm/", http.FileServer(http.Dir("./wasm"))))
		} else {
			r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprintf(w, "<html><head><script>window.location='index.html';</script></head><body><pre><a href='index.html'>index.html</a></pre></body></html>")
			})
			r.HandleFunc("/api-mysql.wasm", Binario.Api)
			r.HandleFunc("/icon.png", Binario.Icon)
			r.HandleFunc("/spinner_dark.gif", Binario.Spinnerdark)
			r.HandleFunc("/spinner_light.gif", Binario.Spinnerlight)
			r.HandleFunc("/index.html", Binario.Index)
			r.HandleFunc("/light.css", Binario.Light)
			r.HandleFunc("/dark.css", Binario.Dark)
			r.HandleFunc("/wasm_exec.js", Binario.Wasm)
			r.HandleFunc("/webgl-debug.js", Binario.Webgl)
		}
	}
	r.HandleFunc("/On", On)
	r.HandleFunc("/Off", Off)
	r.HandleFunc("/API/{server}", showdatabases)
	r.HandleFunc("/API/{server}/", showdatabases)
	r.HandleFunc("/API/{server}/{database}", showtables)
	r.HandleFunc("/API/{server}/{database}/", showtables)
	r.HandleFunc("/API/{server}/{database}/{table}", selectfrom)
	r.HandleFunc("/API/{server}/{database}/{table}/", selectfrom)
	fmt.Println("Servicio en http://" + HostName)
	fmt.Println(http.ListenAndServe(":"+Puerto, r))
	StatusServer = false
	os.Exit(3)
}

func DirExists(filename string) bool {
	info, err := os.Stat(filename)
	if err != nil {
		return false
	}
	return info.IsDir()
}

func On(w http.ResponseWriter, r *http.Request) {
	user, pass, ok := r.BasicAuth()
	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("No contiene las credenciales para cambiar el estado\n"))
		return
	}
	if user != Usuario || pass != Clave {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Credenciales invalidas\n"))
		return
	}
	StatusServer = true
	w.Write([]byte("Activado"))
	return
}

func Off(w http.ResponseWriter, r *http.Request) {
	user, pass, ok := r.BasicAuth()
	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("No contiene las credenciales para cambiar el estado\n"))
		return
	}
	if user != Usuario || pass != Clave {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Credenciales invalidas\n"))
		return
	}
	StatusServer = false
	w.Write([]byte("Desactivado"))
	return
}

func showdatabases(w http.ResponseWriter, r *http.Request) {
	if StatusServer == false {
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte("{\n\t\"Estado\": \"Servidor Desactivado\"\n}"))
		return
	}
	user, pass, ok := r.BasicAuth()
	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("No contiene las credenciales para la base de datos\n"))
		return
	}
	vars := mux.Vars(r)
	server := vars["server"]
	conexion := user + ":" + pass + "@tcp(" + server + ":3306)/mysql?charset=utf8"

	query := r.FormValue("sql")
	if len(query) <= 0 {
		query = "SHOW DATABASES;"
	}

	errores, respuesta := sqlrun(query, conexion)
	if errores == 0 {
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(respuesta + "\n"))
		fmt.Println(query)
		return
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(respuesta))
		return
	}
}

func showtables(w http.ResponseWriter, r *http.Request) {
	if StatusServer == false {
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte("{\n\t\"Estado\": \"Servidor Desactivado\"\n}"))
		return
	}
	user, pass, ok := r.BasicAuth()
	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("No contiene las credenciales para la base de datos\n"))
		return
	}
	vars := mux.Vars(r)
	server := vars["server"]
	database := vars["database"]
	conexion := user + ":" + pass + "@tcp(" + server + ":3306)/" + database + "?charset=utf8"

	query := r.FormValue("sql")
	if len(query) <= 0 {
		query = "SHOW TABLES;"
	}

	errores, respuesta := sqlrun(query, conexion)
	if errores == 0 {
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(respuesta + "\n"))
		fmt.Println(query)
		return
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(respuesta))
		return
	}
}

func selectfrom(w http.ResponseWriter, r *http.Request) {
	if StatusServer == false {
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte("{\n\t\"Estado\": \"Servidor Desactivado\"\n}"))
		return
	}
	user, pass, ok := r.BasicAuth()
	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("No contiene las credenciales para la base de datos\n"))
		return
	}
	vars := mux.Vars(r)
	server := vars["server"]
	database := vars["database"]
	table := vars["table"]
	conexion := user + ":" + pass + "@tcp(" + server + ":3306)/" + database + "?charset=utf8"

	query := r.FormValue("sql")
	if len(query) <= 0 {
		if table == "PROCEDURES" || table == "FUNCTIONS" || table == "VIEWS" || table == "PROGRAMS" {
			query = "SELECT mp.type, CONCAT(mp.db,'.',mp.name,'(',mp.param_list,')') AS 'PROGRAMS' FROM mysql.proc mp WHERE mp.db='" + database + "' UNION SELECT 'VIEW' AS 'type', concat(it.TABLE_SCHEMA,'.',it.TABLE_NAME) AS 'PROGRAMS' FROM information_schema.tables it WHERE it.TABLE_TYPE LIKE 'VIEW' AND it.TABLE_SCHEMA='" + database + "';"
		} else {
			query = "SELECT * FROM " + table + ";"
		}
	}

	errores, respuesta := sqlrun(query, conexion)
	if errores == 0 {
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(respuesta + "\n"))
		fmt.Println(query)
		return
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(respuesta))
		return
	}
}

func sqlrun(query, conexion string) (int, string) {
	respuesta := "["
	db, err := sql.Open("mysql", conexion)
	if err != nil {
		db.Close()
		return 1, "No se logro aperturar conexion a: " + conexion + "\n"
	}
	defer db.Close()
	rows, err := db.Query(query)
	if err != nil {
		db.Close()
		rows.Close()
		return 1, "No se logro ejecutar la query: " + query + "\n"
	}
	defer rows.Close()

	llaves := 0

	for {
		colTypes, err := rows.ColumnTypes()
		if err != nil {
			db.Close()
			rows.Close()
			return 1, "No se logro obtener el tipado al ejecutar la query: " + query + "\n"
		}		
		columns, err := rows.Columns()
		if err != nil {
			db.Close()
			rows.Close()
			return 1, "No se logro obtener respuesta al ejecutar la query: " + query + "\n"
		}
		values := make([]sql.RawBytes, len(columns))
		scanArgs := make([]interface{}, len(values))
		for i := range values {
			scanArgs[i] = &values[i]
		}

		for rows.Next() {
			err = rows.Scan(scanArgs...)
			if err != nil {
				db.Close()
				rows.Close()
				return 1, "No se logro obtener datos al ejecutar la query: " + query + "\n"
			}
			if llaves == 0 {
				respuesta = respuesta + "\n\t{"
				llaves = llaves + 1
			} else {
				respuesta = respuesta + ",\n\t{"
			}
			campos := 0
			var value string
			for i, col := range values {
				if col == nil {
					value = ""
				} else {
					if(strings.Contains(colTypes[i].DatabaseTypeName(), "BLOB")){
						value = base64.StdEncoding.EncodeToString(col)
					} else {
						value = string(col)
						value = strings.ReplaceAll(value, "\"", "")
						value = strings.ReplaceAll(value, "\n", "\\n")
					}
				}
				if campos == 0 {
					respuesta = respuesta + "\n\t\t\"" + strings.ReplaceAll(strings.ReplaceAll(columns[i], "\"", ""), "\n", "\\n") + "\": " + "\"" + value + "\""
					campos = campos + 1
				} else {
					respuesta = respuesta + ", \n\t\t\"" + strings.ReplaceAll(strings.ReplaceAll(columns[i], "\"", ""), "\n", "\\n") + "\": " + "\"" + value + "\""
				}
			}
			respuesta = respuesta + "\n\t}"
		}

		if !rows.NextResultSet() {
			break
		}
	}
	db.Close()
	rows.Close()
	respuesta = respuesta + "\n]"

	if respuesta == "[\n]" && strings.HasPrefix(query, "CALL ") {
		return 0, "{\n\t\"EJECUCION\": \"OK\"\n}"
	} else {
		return 0, respuesta
	}
}
