# Api Rest MySQL/MariaDB

Servidor basico para compartir data mediante una Api Rest, te permitira crear consultas y ejecutar query, como crear curl de consulta para importarse en Postman. Este servidor se construyó con la librería Fyne y usando el lenguaje de programación Go para permitir la compatibilidad con los sistemas operativos: FreeBSD, Linux, MacOS, Windows y Android.

También puedes usar este programa sin interfaz gráfica usando el argumento del puerto en el que dar servicio.


<p align="center">
  <img alt="Light" src="https://gitlab.com/RicardoValladares/api-mysql/-/raw/main/capturas%20de%20pantalla/movil.png" width="25%"> &nbsp; <img alt="Light" src="https://gitlab.com/RicardoValladares/api-mysql/-/raw/main/capturas%20de%20pantalla/escritorio.png" width="65%">
</p>


<p align="center">
  <img alt="Light" src="https://gitlab.com/RicardoValladares/api-mysql/-/raw/main/capturas%20de%20pantalla/terminal.png" width="40%"> &nbsp; <img alt="Light" src="https://gitlab.com/RicardoValladares/api-mysql/-/raw/main/capturas%20de%20pantalla/web.png" width="55%">
 </p>


## Instalar desde el compilador Go:

```bash
go install gitlab.com/RicardoValladares/api-mysql@latest
```

## Ejecutar con interfaz gráfica:

```bash
api-mysql
```

## Ejecutar sin interfaz gráfica, especificando puerto:

```bash
api-mysql 8080
```

### SHOW DATABASES:

```bash
curl --location --request POST "http://localhost:8080/API/127.0.0.1" --header "Authorization: Basic cm9vdDoxMjM0NTY="
```

### SHOW TABLES:

```bash
curl --location --request POST "http://localhost:8080/API/127.0.0.1/mysql" --header "Authorization: Basic cm9vdDoxMjM0NTY="
```

### SELECT * FROM TABLE:

```bash
curl --location --request POST "http://localhost:8080/API/127.0.0.1/mysql/user" --header "Authorization: Basic cm9vdDoxMjM0NTY="
```

### SELECT personalizado:

```bash
curl --location --request POST "http://localhost:8080/API/127.0.0.1/contactos" --header "Authorization: Basic cm9vdDoxMjM0NTY=" --data "sql=SELECT usuarios, mensajes, TO_BASE64(fotos) FROM mensajeria"
```

### INSERT personalizado:

```bash
curl --location --request POST "http://localhost:8080/API/127.0.0.1/contactos" --header "Authorization: Basic cm9vdDoxMjM0NTY=" --data "sql=INSERT INTO mensajeria(usuarios, mensajes) VALUES('RICARDO','Hola Mundo')"
```

### SHOW PROCEDURES, FUNCTIONS, VIEWS:

```bash
curl --location --request POST "http://localhost:8080/API/127.0.0.1/mysql/PROGRAMS" --header "Authorization: Basic cm9vdDoxMjM0NTY="
```

