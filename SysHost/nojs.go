
// file nojs.go

// +build !js

package SysHost

import "net"
import "encoding/base64"
import Global "gitlab.com/RicardoValladares/api-mysql/SysHost/Variables"

func HostName() string{
	return Global.Host+":"+Global.Puerto
}

func Host() string{
	return Global.Host
}

func Puerto() string{
	return Global.Puerto
}

func Usuario() string{
	return Global.Usuario
}

func Clave() string{
	return Global.Clave
}

func Credenciales() string {
	return base64.StdEncoding.EncodeToString([]byte(Global.Usuario+":"+Global.Clave))
}

func GetCredenciales(user, pass string) string{
	return base64.StdEncoding.EncodeToString([]byte(user+":"+pass))
}

func SetPuerto(port string){
	Global.Puerto = port
}

func init() {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		Global.Host = "127.0.0.1"
		return
	}
	defer conn.Close()
	localAddress := conn.LocalAddr().(*net.UDPAddr)
	Global.Host = localAddress.IP.String()
}