
// file js.go

// +build js

package SysHost

import "encoding/base64"
import Global "gitlab.com/RicardoValladares/api-mysql/SysHost/Variables"

import (
	"syscall/js"
)

func HostName() string{
	return js.Global().Get("location").Get("host").String()
}

func Host() string{
	return Global.Host
}

func Puerto() string{
	return Global.Puerto
}

func Usuario() string{
	return Global.Usuario
}

func Clave() string{
	return Global.Clave
}

func Credenciales() string {
	return base64.StdEncoding.EncodeToString([]byte(Global.Usuario+":"+Global.Clave))
}

func GetCredenciales(user, pass string) string{
	return base64.StdEncoding.EncodeToString([]byte(user+":"+pass))
}

func SetPuerto(port string){
	Global.Puerto = port
}

