package main

import (
	"io/ioutil"
	"net/http"
	"os"
	"fmt"
	"os/exec"
	Endpoint "gitlab.com/RicardoValladares/api-mysql/SysHost"
	Servidor "gitlab.com/RicardoValladares/api-mysql/Server"
)


func main() {
	if len(os.Args) == 2 {
		Endpoint.SetPuerto(os.Args[1])
		Servidor.Run(Endpoint.HostName(), Endpoint.Puerto(), Endpoint.Usuario(), Endpoint.Clave())
	} else {

		cmd := exec.Command("am", "start", "--user", "0", "-n", "gitlab.com.ricardovalladares.apimysql/org.golang.app.GoNativeActivity")
		err := cmd.Run()
		if err != nil {
			if exec.Command("termux-open-url", "https://github.com/RicardoValladares/Golang/releases/download/1.0/api_mysql.apk").Run() != nil {
				fmt.Println("No se puede ejecutar interfaz grafica, para usar la consola pon un puerto")
			}
		}
	
	}
}

func Detener() string {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "http://"+Endpoint.HostName()+"/Off", nil)
	req.SetBasicAuth(Endpoint.Usuario(), Endpoint.Clave())
	response, err := client.Do(req)
	if err != nil {
		return "Error de comunicacion"
	}
	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "Error de comunicacion"
	}
	return string(responseData)
}

func Iniciar() string {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "http://"+Endpoint.HostName()+"/On", nil)
	req.SetBasicAuth(Endpoint.Usuario(), Endpoint.Clave())
	response, err := client.Do(req)
	if err != nil {
		return "Error de comunicacion"
	}
	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "Error de comunicacion"
	}
	return string(responseData)
}
